#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

loadkeys trq
mount -o remount,size=1G /run/archiso/cowspace
pacman -Sy --noconfirm git
git clone --depth 1 http://dev.acmegen.com:16524/scm/pos/linuxbaker.git
cd linuxbaker
./posnix.sh
