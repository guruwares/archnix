#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

X11_COUNTRY="tr"
X11_KEYMAP="pc102"

PARTITIONTABLETYPE="o";#fdisk o-dos,g-gpt
total_memory=`grep MemTotal /proc/meminfo | awk '{print $2/1024}' | sed 's/\..*//'`
SWAPSIZE="1024M"
profile_packages=""
desktop="lxde"
PROFILEVIDEO_DRIVER="xf86-video-openchrome mesa-libgl libvdpau-va-gl"

profile_print_menuitems(){
}

profile_handle_menuitem(){
}
