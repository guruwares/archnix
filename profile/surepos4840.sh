#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

X11_COUNTRY="tr"
X11_KEYMAP="pc102"

source `pwd`/attach/surepos_elo_touch.sh

PARTITIONTABLETYPE="o";#fdisk o-dos,g-gpt
total_memory=`grep MemTotal /proc/meminfo | awk '{print $2/1024}' | sed 's/\..*//'`
SWAPSIZE=$(echo "${total_memory}*2" | bc)M
profile_packages=""
desktop="lxde"
PROFILEVIDEO_DRIVER="xf86-video-vesa mesa-libgl libvdpau-va-gl"

profile_print_menuitems(){
  echo "20) $(mainmenu_item "${checklist[9]}"  "Install EloTouch Serial" )"
}

profile_handle_menuitem(){
  start=$1
  itm=$2
  case "$itm" in
  20)
      print_title "SurePos EloTouch Serial Port Selection (ttyS4 önerilir)"
      ports_list=(`dmesg | grep tty[sS] | awk '{print $4}'`);
      echo "Found ports $ports_list"
      PS3="$prompt1"
      echo -e "Attached Devices:\n"
      select portname in "${ports_list[@]}"; do
        if contains_element "${portname}" "${ports_list[@]}"; then
          install_elotouch_serial $portname
          break
        else
          invalid_option
        fi
      done
      ;;
  *)
    invalid_option
    ;;
  esac
}
