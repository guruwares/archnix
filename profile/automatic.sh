#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

X11_COUNTRY="tr"
X11_KEYMAP="pc102"

PARTITIONTABLETYPE="o";#fdisk o-dos,g-gpt
total_memory=`grep MemTotal /proc/meminfo | awk '{print $2/1024}' | sed 's/\..*//'`
SWAPSIZE=$(echo "${total_memory}*2" | bc)M
profile_packages=""
desktop="lxde"

profile_print_menuitems(){
  echo ""
}

profile_handle_menuitem(){
  invalid_option
}
