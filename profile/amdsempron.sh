#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

X11_COUNTRY="tr"
X11_KEYMAP="pc102"

PARTITIONTABLETYPE="o";#fdisk o-dos,g-gpt
SWAPSIZE="2048M"
profile_packages=""
desktop="lxde"
PROFILEVIDEO_DRIVER="xf86-video-vesa mesa-libgl libvdpau-va-gl"

profile_print_menuitems(){
}

profile_handle_menuitem(){
  invalid_option
}
