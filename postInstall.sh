#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

PROFILE=$(</root/install_profile)
INSTALL_FROM=$(</root/install_from)
username=$(</root/install_username)

if [[ -f `pwd`/sharedfuncs.sh ]]; then
  source sharedfuncs.sh
else
  echo "missing file: sharedfuncs"
  exit 1
fi

if [[ -f `pwd`/profile/"$PROFILE".sh ]]; then
  source profile/"$PROFILE".sh
else
  echo "missing file: profile/"$PROFILE".sh"
  exit 1
fi

#ARCHLINUX U INSTALL {{{
#WELCOME {{{
welcome(){
  clear
  echo -e "${Bold}Welcome to the ARCHNIX Install script${White}"
  print_line
  echo "Requirements:"
  echo "-> Archlinux installation"
  echo "-> Run script as root user"
  echo "-> Working internet connection"
  print_line
  echo "installation can be cancelled at any time with CTRL+C"
  print_line
  echo ""
}
#}}}
init_locale() {
  LOCALE=`locale | grep LANG | sed 's/LANG=//' | cut -c1-5`
  #KDE #{{{
  if [[ $LOCALE == pt_BR || $LOCALE == en_GB || $LOCALE == zh_CN ]]; then
    LOCALE_KDE=`echo $LOCALE | tr '[:upper:]' '[:lower:]'`
  else
    LOCALE_KDE=`echo $LOCALE | cut -d\_ -f1`
  fi
  #}}}
  #FIREFOX #{{{
  if [[ $LOCALE == pt_BR || $LOCALE == pt_PT || $LOCALE == en_GB || $LOCALE == en_US || $LOCALE == es_AR || $LOCALE == es_CL || $LOCALE == es_ES || $LOCALE == zh_CN ]]; then
    LOCALE_FF=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
  else
    LOCALE_FF=`echo $LOCALE | cut -d\_ -f1`
  fi
  #}}}
  #THUNDERBIRD #{{{
  if [[ $LOCALE == pt_BR || $LOCALE == pt_PT || $LOCALE == en_US || $LOCALE == en_GB || $LOCALE == es_AR || $LOCALE == es_ES || $LOCALE == zh_CN ]]; then
    LOCALE_TB=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
  elif [[ $LOCALE == es_CL ]]; then
    LOCALE_TB="es-es"
  else
    LOCALE_TB=`echo $LOCALE | cut -d\_ -f1`
  fi
  #}}}
  #HUNSPELL #{{{
  if [[ $LOCALE == pt_BR ]]; then
    LOCALE_HS=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
  elif [[ $LOCALE == pt_PT ]]; then
    LOCALE_HS="pt_pt"
  else
    LOCALE_HS=`echo $LOCALE | cut -d\_ -f1`
  fi
  #}}}
  #ASPELL #{{{
  LOCALE_AS=`echo $LOCALE | cut -d\_ -f1`
  #}}}
  #LIBREOFFICE #{{{
  if [[ $LOCALE == pt_BR || $LOCALE == en_GB || $LOCALE == en_US || $LOCALE == zh_CN ]]; then
    LOCALE_LO=`echo $LOCALE | sed 's/_/-/'`
  else
    LOCALE_LO=`echo $LOCALE | cut -d\_ -f1`
  fi
  #}}}
  #}}}
}
#LOCALE SELECTOR {{{
language_selector(){
  #AUTOMATICALLY DETECTS THE SYSTEM LOCALE {{{
  #automatically detects the system language based on your locale
  init_locale
  print_title "LOCALE - https://wiki.archlinux.org/index.php/Locale"
  print_info "Locales are used in Linux to define which language the user uses. As the locales define the character sets being used as well, setting up the correct locale is especially important if the language contains non-ASCII characters."
  read -p "Default system language: \"$LOCALE\" [Y/n]: " OPTION
  case "$OPTION" in
    "n")
      while [[ $OPTION != y ]]; do
        setlocale
        read_input_text "Confirm locale ($LOCALE)"
      done
      sed -i '/'${LOCALE}'/s/^#//' /etc/locale.gen
      locale-gen
      localectl set-locale LANG=${LOCALE_UTF8}
      #KDE #{{{
      if [[ $LOCALE == pt_BR || $LOCALE == en_GB || $LOCALE == zh_CN ]]; then
        LOCALE_KDE=`echo $LOCALE | tr '[:upper:]' '[:lower:]'`
      else
        LOCALE_KDE=`echo $LOCALE | cut -d\_ -f1`
      fi
      #}}}
      #FIREFOX #{{{
      if [[ $LOCALE == pt_BR || $LOCALE == pt_PT || $LOCALE == en_GB || $LOCALE == en_US || $LOCALE == es_AR || $LOCALE == es_CL || $LOCALE == es_ES || $LOCALE == zh_CN ]]; then
        LOCALE_FF=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
      else
        LOCALE_FF=`echo $LOCALE | cut -d\_ -f1`
      fi
      #}}}
      #THUNDERBIRD #{{{
      if [[ $LOCALE == pt_BR || $LOCALE == pt_PT || $LOCALE == en_US || $LOCALE == en_GB || $LOCALE == es_AR || $LOCALE == es_ES || $LOCALE == zh_CN ]]; then
        LOCALE_TB=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
      elif [[ $LOCALE == es_CL ]]; then
        LOCALE_TB="es-es"
      else
        LOCALE_TB=`echo $LOCALE | cut -d\_ -f1`
      fi
      #}}}
      #HUNSPELL #{{{
      if [[ $LOCALE == pt_BR ]]; then
        LOCALE_HS=`echo $LOCALE | tr '[:upper:]' '[:lower:]' | sed 's/_/-/'`
      elif [[ $LOCALE == pt_PT ]]; then
        LOCALE_HS="pt_pt"
      else
        LOCALE_HS=`echo $LOCALE | cut -d\_ -f1`
      fi
      #}}}
      #ASPELL #{{{
      LOCALE_AS=`echo $LOCALE | cut -d\_ -f1`
      #}}}
      #LIBREOFFICE #{{{
      if [[ $LOCALE == pt_BR || $LOCALE == en_GB || $LOCALE == en_US || $LOCALE == zh_CN ]]; then
        LOCALE_LO=`echo $LOCALE | sed 's/_/-/'`
      else
        LOCALE_LO=`echo $LOCALE | cut -d\_ -f1`
      fi
      #}}}
      ;;
    *)
      ;;
  esac
}
#}}}
#CONFIGURE SUDO {{{
configure_sudo(){
  if ! is_package_installed "sudo" ; then
    print_title "SUDO - https://wiki.archlinux.org/index.php/Sudo"
    package_install "sudo"
  fi
  #CONFIGURE SUDOERS {{{
  if [[ ! -f  /etc/sudoers.aui ]]; then
    cp -v /etc/sudoers /etc/sudoers.aui
    ## Uncomment to allow members of group wheel to execute any command
    sed -i '/%wheel ALL=(ALL) ALL/s/^#//' /etc/sudoers
    ## Same thing without a password (not secure)
    sed -i 's/#includedir /${username} ALL=(ALL) ALL\n#includedir /g' /etc/sudoers

    #This config is especially helpful for those using terminal multiplexers like screen, tmux, or ratpoison, and those using sudo from scripts/cronjobs:
    echo "" >> /etc/sudoers
    echo "Cmnd_Alias PGCMDS=/usr/bin/timedatectl,/bin/date,/bin/mount,/bin/umount,/usr/bin/teamviewer,/usr/bin/anydesk" >> /etc/sudoers
    echo "${username} ALL=(ALL) NOPASSWD: PGCMDS" >> /etc/sudoers
    echo "" >> /etc/sudoers
    echo 'Defaults !requiretty, !tty_tickets, !umask' >> /etc/sudoers
    echo 'Defaults visiblepw, path_info, insults, lecture=always' >> /etc/sudoers
    echo 'Defaults loglinelen=0, logfile =/var/log/sudo.log, log_year, log_host, syslog=auth' >> /etc/sudoers
    echo 'Defaults passwd_tries=3, passwd_timeout=1' >> /etc/sudoers
    echo 'Defaults env_reset, always_set_home, set_home, set_logname' >> /etc/sudoers
    echo 'Defaults !env_editor, editor="/usr/bin/vim:/usr/bin/vi:/usr/bin/nano"' >> /etc/sudoers
    echo 'Defaults timestamp_timeout=15' >> /etc/sudoers
    echo 'Defaults passprompt="[sudo] password for %u: "' >> /etc/sudoers
  fi
  #}}}
}
#}}}
#BASIC SETUP {{{
install_basic_setup(){
  if ! is_package_installed "yaourt" ; then
    rm -rf ~/.yaourtrc
    cp .yaourtrc ~/
    rm -rf /home/${username}/.yaourtrc
    cp .yaourtrc /home/${username}/
    chown "$username":users /home/${username}/.yaourtrc
    pacman -D --asdeps yajl namcap
    archnix_install_yaourt
    if ! is_package_installed "yaourt" ; then
      echo "Yaourt not installed. EXIT now"
      pause_function
      exit 0
    fi
  fi

  package_bulkinstall "base-devel bc rsync mlocate bash-completion pkgstats zip unzip unrar p7zip lzop cpio avahi nss-mdns hdparm wget"
  package_bulkinstall "alsa-utils alsa-plugins pulseaudio pulseaudio-alsa ntfs-3g dosfstools exfat-utils f2fs-tools fuse2 exfat-utils autofs mtpfs nfs-utils"
  package_install "cifs-utils"

  if is_package_installed "nfs-utils" ; then
    system_ctl enable rpcbind
    system_ctl enable nfs-client.target
    system_ctl enable remote-fs.target
  fi
  is_package_installed "avahi" && system_ctl enable avahi-daemon

  package_bulkinstall "xorg-server xorg-xinit xterm xorg-xclock xorg-twm xorg-apps xorg-xhost xorg-xkill xorg-xinput xf86-input-libinput"
  modprobe uinput
}
#}}}
install_video_cards(){
  package_install "dmidecode"
  print_title "VIDEO CARD"
  check_vga
  #Virtualbox {{{
  if [[ ${VIDEO_DRIVER} == virtualbox ]]; then
    package_bulkinstall "virtualbox-guest-modules-arch virtualbox-guest-utils mesa-libgl"
    add_module "vboxguest vboxsf vboxvideo" "virtualbox-guest"
    add_user_to_group ${username} vboxsf
    system_ctl disable ntpd
    system_ctl enable vboxservice
  #}}}
  #VMware {{{
  elif [[ ${VIDEO_DRIVER} == vmware ]]; then
    package_bulkinstall "xf86-video-vmware xf86-input-vmmouse open-vm-tools"
    cat /proc/version > /etc/arch-release
    system_ctl disable ntpd
    system_ctl enable vmtoolsd
  #}}}
  #Bumblebee {{{
  elif [[ ${VIDEO_DRIVER} == bumblebee ]]; then
    XF86_DRIVERS=$(pacman -Qe | grep xf86-video | awk '{print $1}')
    [[ -n $XF86_DRIVERS ]] && pacman -Rcsn $XF86_DRIVERS
    pacman -S --needed xf86-video-intel bumblebee nvidia
    [[ ${ARCHI} == x86_64 ]] && pacman -S --needed lib32-virtualgl lib32-nvidia-utils
    replace_line '*options nouveau modeset=1' '#options nouveau modeset=1' /etc/modprobe.d/modprobe.conf
    replace_line '*MODULES="nouveau"' '#MODULES="nouveau"' /etc/mkinitcpio.conf
    mkinitcpio -p linux
    add_user_to_group ${username} bumblebee
  #}}}
  #NVIDIA {{{
  elif [[ ${VIDEO_DRIVER} == nvidia ]]; then
    XF86_DRIVERS=$(pacman -Qe | grep xf86-video | awk '{print $1}')
    [[ -n $XF86_DRIVERS ]] && pacman -Rcsn $XF86_DRIVERS
    pacman -S --needed nvidia{,-utils}
    [[ ${ARCHI} == x86_64 ]] && pacman -S --needed lib32-nvidia-utils
    replace_line '*options nouveau modeset=1' '#options nouveau modeset=1' /etc/modprobe.d/modprobe.conf
    replace_line '*MODULES="nouveau"' '#MODULES="nouveau"' /etc/mkinitcpio.conf
    mkinitcpio -p linux
    nvidia-xconfig --add-argb-glx-visuals --allow-glx-with-composite --composite -no-logo --render-accel -o /etc/X11/xorg.conf.d/20-nvidia.conf;
  #}}}
  #Nouveau [NVIDIA] {{{
  elif [[ ${VIDEO_DRIVER} == nouveau ]]; then
    is_package_installed "nvidia" && pacman -Rdds --noconfirm nvidia{,-utils}
    [[ ${ARCHI} == x86_64 ]] && is_package_installed "lib32-nvidia-utils" && pacman -Rdds --noconfirm lib32-nvidia-utils
    [[ -f /etc/X11/xorg.conf.d/20-nvidia.conf ]] && rm /etc/X11/xorg.conf.d/20-nvidia.conf
    package_bulkinstall "xf86-video-${VIDEO_DRIVER} mesa-libgl libvdpau-va-gl"
    replace_line '#*options nouveau modeset=1' 'options nouveau modeset=1' /etc/modprobe.d/modprobe.conf
    replace_line '#*MODULES="nouveau"' 'MODULES="nouveau"' /etc/mkinitcpio.conf
    mkinitcpio -p linux
  #}}}
  #ATI {{{
  elif [[ ${VIDEO_DRIVER} == ati ]]; then
    is_package_installed "catalyst-total" && pacman -Rdds --noconfirm catalyst-total
    [[ -f /etc/X11/xorg.conf.d/20-radeon.conf ]] && rm /etc/X11/xorg.conf.d/20-radeon.conf
    [[ -f /etc/modules-load.d/catalyst.conf ]] && rm /etc/modules-load.d/catalyst.conf
    [[ -f /etc/X11/xorg.conf ]] && rm /etc/X11/xorg.conf
    package_bulkinstall "xf86-video-${VIDEO_DRIVER} mesa-libgl mesa-vdpau libvdpau-va-gl"
    add_module "radeon" "ati"
  #}}}
  #Intel {{{
  elif [[ ${VIDEO_DRIVER} == intel ]]; then
    package_bulkinstall "xf86-video-${VIDEO_DRIVER} mesa-libgl libvdpau-va-gl"
  #}}}
  #Profile {{{
  elif [[ ${VIDEO_DRIVER} == "@" ]]; then
    package_bulkinstall "${PROFILEVIDEO_DRIVER}"
  #}}}
  #Vesa {{{
  else
    package_bulkinstall "xf86-video-${VIDEO_DRIVER} mesa-libgl libvdpau-va-gl"
  fi
  #}}}
  if [[ ${ARCHI} == x86_64 ]]; then
    is_package_installed "mesa-libgl" && package_install "lib32-mesa-libgl"
    is_package_installed "mesa-vdpau" && package_install "lib32-mesa-vdpau"
  fi
  if is_package_installed "libvdpau-va-gl"; then
    add_line "export VDPAU_DRIVER=va_gl" "/etc/profile"
  fi
}
#}}}
#CONNMAN/NETWORKMANAGER/WICD {{{
install_nm_wicd(){
  if [[ ${KDE} -eq 1 ]]; then
    package_bulkinstall "networkmanager dnsmasq plasma-nm networkmanager-qt"
  else
    package_bulkinstall "networkmanager dnsmasq network-manager-applet nm-connection-editor gnome-keyring"
  fi
  # auto update datetime from network
  if is_package_installed "ntp"; then
    package_install "networkmanager-dispatcher-ntpd"
    system_ctl enable NetworkManager-dispatcher.service
  fi
  # power manager support
  #is_package_installed "tlp" && package_install "tlp-rdw"
  # network management daemon
  system_ctl enable NetworkManager.service
}
#}}}
#CLEAN ORPHAN PACKAGES {{{
clean_orphan_packages(){
  print_title "CLEAN ORPHAN PACKAGES"
  pacman -Rsc --noconfirm $(pacman -Qqdt)
  #pacman -Sc --noconfirm
  pacman-optimize
}
#}}}
#INSTALL GUI {{{
install_gui(){
  echo "Installing desktop $desktop"

  rm -rf /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "Section \"InputClass\"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "Identifier \"system-keyboard\"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "MatchIsKeyboard \"on\"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "Option \"XkbLayout\" \"$X11_COUNTRY\"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "Option \"XkbModel\" \"$X11_KEYMAP\"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
  echo "EndSection" >> /etc/X11/xorg.conf.d/00-keyboard.conf

  rm -rf /home/$username/.bash_profile
  cp /etc/skel/.bash_profile /home/$username/.bash_profile
  add_line "[[ -z \$DISPLAY && \$XDG_VTNR -eq 1 ]] && exec startx /home/$username/.xinitrc ${desktop}" "/home/$username/.bash_profile"
  chown "$username":users /home/$username/.bash_profile

  rm -rf /home/${username}/.xinitrc
  echo "#!/bin/sh" >> /home/$username/.xinitrc
  echo "" >> /home/$username/.xinitrc
  echo "session=${1:-${desktop}}" >> /home/$username/.xinitrc
  echo "case \$session in" >> /home/$username/.xinitrc
  echo "  lxde) exec startlxde;;" >> /home/$username/.xinitrc
  echo "  lxqt) exec startlxqt;;" >> /home/$username/.xinitrc
  echo "  cinnamon) exec cinnamon-session;;" >> /home/$username/.xinitrc
  echo "  mate) exec mate-session;;" >> /home/$username/.xinitrc
  echo "  xfce|xfce4) exec startxfce4;;" >> /home/$username/.xinitrc
  echo "  deepin) exec startdde;;" >> /home/$username/.xinitrc
  echo "  *) exec \$1;;" >> /home/$username/.xinitrc
  echo "esac" >> /home/$username/.xinitrc
  chown "$username":users /home/${username}/.xinitrc

  case $desktop in
    lxde)
      package_bulkinstall "lxde ttf-dejavu leafpad obconf gnome-screenshot gnome-calculator gvfs xarchiver firefox firefox-i18n-$LOCALE_FF"
      localectl --no-convert set-x11-keymap $X11_COUNTRY
      rm -rf /home/${username}/.config/lxsession
      mkdir -p /home/${username}/.config/lxsession
      cp -r /etc/xdg/lxsession/LXDE /home/${username}/.config/lxsession
      chown -R ${username}:users /home/${username}/.config/lxsession/LXDE

      #add_line "setterm -blank 0 -powerdown 0" /home/${username}/.config/lxsession/LXDE/autostart
      #add_line "xset s off && xset -dpms" /home/${username}/.config/lxsession/LXDE/autostart
      #chown -R "$username":users /home/${username}/.config/lxsession/LXDE

      package_install "xfce4-power-manager"
      rm -rf /home/${username}/.config/xfce4
      mkdir -p /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml
      echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" >> /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "<channel name=\"xfce4-power-manager\" version=\"1.0\">" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "<property name=\"xfce4-power-manager\" type=\"empty\">" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "<property name=\"dpms-enabled\" type=\"bool\" value=\"false\"/>" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "<property name=\"blank-on-ac\" type=\"int\" value=\"0\"/>" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "</property>" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      add_line "</channel>" /home/${username}/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
      chown -R "$username":users /home/${username}/.config/xfce4

      install_theme_default
    ;;
    lxqt)
      package_bulkinstall "lxqt openbox oxygen-icons qtcurve leafpad firefox firefox-i18n-$LOCALE_FF"
      mkdir -p /home/${username}/.config/lxqt
      cp /etc/xdg/lxqt/* /home/${username}/.config/lxqt
      mkdir -p /home/${username}/.config/openbox/
      cp /etc/xdg/openbox/{menu.xml,rc.xml,autostart} /home/${username}/.config/openbox/
      chown -R ${username}:users /home/${username}/.config
      config_xinitrc ""
    ;;
    cinnamon)
      package_bulkinstall "cinnamon nemo-fileroller nemo-preview xarchiver firefox firefox-i18n-$LOCALE_FF"
      install_theme_default
    ;;
    deepin)
      package_bulkinstall "deepin deepin-extra xarchiver firefox firefox-i18n-$LOCALE_FF"
      install_theme_default
    ;;
    xfce4)
      package_bulkinstall "xfce4 xfce4-goodies xarchiver firefox firefox-i18n-$LOCALE_FF"
    ;;
    mate)
      package_bulkinstall "deepin deepin-extra xarchiver firefox firefox-i18n-$LOCALE_FF"
    ;;
    *)
      invalid_option
      pause_function
      ;;
  esac
}
#}}}
#INSTALL GUI {{{
install_qt5(){
  if [[ $INSTALL_FROM == "r" ]]; then
    #ARCH_ARCHIVE="https://archive.archlinux.org/packages/"
    QT_VERSION="5.7.1-1"
    QT_DIR="/opt/qt571"
    mkdir -p $QT_DIR
    rm -rf *.tar.xz
    wget "${ARCHNIX_DOWNLOADS}/i/icu/icu-58.1-1-${ARCHI}.pkg.tar.xz" -O $QT_DIR/icu-58.1-1.tar.xz
    wget "${ARCHNIX_DOWNLOADS}/q/qt5-base/qt5-base-${QT_VERSION}-${ARCHI}.pkg.tar.xz" -O $QT_DIR/qt5-base-${QT_VERSION}.tar.xz
    wget "${ARCHNIX_DOWNLOADS}/q/qt5-serialport/qt5-serialport-${QT_VERSION}-${ARCHI}.pkg.tar.xz" -O $QT_DIR/qt5-serialport-${QT_VERSION}.tar.xz
    wget "${ARCHNIX_DOWNLOADS}/q/qt5-connectivity/qt5-connectivity-${QT_VERSION}-${ARCHI}.pkg.tar.xz" -O $QT_DIR/qt5-connectivity-${QT_VERSION}.tar.xz
    wget "${ARCHNIX_DOWNLOADS}/q/qt5-multimedia/qt5-multimedia-${QT_VERSION}-${ARCHI}.pkg.tar.xz" -O $QT_DIR/qt5-multimedia-${QT_VERSION}.tar.xz

    tar xvf $QT_DIR/icu-58.1-1.tar.xz -C $QT_DIR
    tar xvf $QT_DIR/qt5-base-${QT_VERSION}.tar.xz -C $QT_DIR
    tar xvf $QT_DIR/qt5-serialport-${QT_VERSION}.tar.xz -C $QT_DIR
    tar xvf $QT_DIR/qt5-connectivity-${QT_VERSION}.tar.xz -C $QT_DIR
    tar xvf $QT_DIR/qt5-multimedia-${QT_VERSION}.tar.xz -C $QT_DIR

    add_line "LD_LIBRARY_PATH=$QT_DIR/usr/lib" "/etc/environment"
    add_line "QT_QPA_PLATFORM_PLUGIN_PATH=$QT_DIR/usr/lib/qt/plugins" "/etc/environment"

    chown -R ${username}:users $QT_DIR

    #package_install "double-conversion" ##offline kurulumda eski surumde 2017.01.01 olduğumuz için bu paket yok
    package_bulkinstall "pcre2 tslib xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm libxkbcommon libxkbcommon-x11"
  else
    #package_install "double-conversion" ##offline kurulumda eski surumde 2017.01.01 olduğumuz için bu paket yok
    package_install "qt5-base"
    package_bulkinstall "qt5-serialport qt5-connectivity qt5-multimedia"
  fi
}
#}}}
#INSTALL GUI {{{
install_unclutter(){
  package_install "unclutter"
  case $desktop in
    lxde)
      mkdir -p /home/${username}/.config/autostart
      echo "[Desktop Entry]" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Name=hidemouse" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "GenericName=hidemouse" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Comment=hidemouse" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Exec=/sbin/unclutter" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Terminal=false" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Type=Application" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Categories=GTK;Office;" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "StartupNotify=true" >> /home/${username}/.config/autostart/hidemouse.desktop
      echo "Name[tr]=hidemouse" >> /home/${username}/.config/autostart/hidemouse.desktop
      chown -R "$username":users /home/${username}/.config
      ;;
    cinnamon)
      ;;
    deepin)
      ;;
    *)
      invalid_option
      ;;
  esac
}
#}}}
#INSTALL THEME {{{
install_theme_paper(){
  aur_package_install "paper-gtk-theme-git paper-icon-theme-git"
  case $desktop in
    lxde)
      mkdir -p /home/${username}/.config/gtk-3.0
      echo "[Settings]" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-theme-name=Paper" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-icon-theme-name=Paper" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-font-name=Sans 10" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-cursor-theme-size=18" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-button-images=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-menu-images=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-enable-event-sounds=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-enable-input-feedback-sounds=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-antialias=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-hinting=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-hintstyle=hintslight" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-rgba=rgb" >> /home/${username}/.config/gtk-3.0/setting.ini

      replace_line "sNet\/ThemeName=.*" "sNet\/ThemeName=Paper" /home/${username}/.config/lxsession/LXDE/desktop.conf
      replace_line "sNet\/IconThemeName=.*" "sNet\/IconThemeName=Paper" /home/${username}/.config/lxsession/LXDE/desktop.conf

      chown -R "$username":users /home/${username}/.config/gtk-3.0
      chown -R "$username":users /home/${username}/.config/lxsession
      ;;
    cinnamon)
      ;;
    deepin)
      ;;
    *)
      invalid_option
      ;;
  esac
}
#}}}
#INSTALL THEME {{{
install_theme_default(){
  case $desktop in
    lxde)
      package_bulkinstall "arc-gtk-theme arc-icon-theme"
      mkdir -p /home/${username}/.config/gtk-3.0
      echo "[Settings]" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-theme-name=Arc-Dark" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-icon-theme-name=Arc" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-font-name=Sans 10" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-cursor-theme-size=18" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-button-images=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-menu-images=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-enable-event-sounds=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-enable-input-feedback-sounds=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-antialias=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-hinting=1" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-hintstyle=hintslight" >> /home/${username}/.config/gtk-3.0/setting.ini
      echo "gtk-xft-rgba=rgb" >> /home/${username}/.config/gtk-3.0/setting.ini

      if [[ -f /home/${username}/.config/lxsession/LXDE/desktop.conf ]]; then
        replace_line "sNet\/ThemeName=.*" "sNet\/ThemeName=Arc-Dark" /home/${username}/.config/lxsession/LXDE/desktop.conf
        replace_line "sNet\/IconThemeName=.*" "sNet\/IconThemeName=Arc" /home/${username}/.config/lxsession/LXDE/desktop.conf
      else
        error_msg "Couldnt find /home/${username}/.config/lxsession/LXDE/desktop.conf"
      fi

      chown -R "$username":users /home/${username}/.config
      ;;
    cinnamon)
      ;;
    deepin)
      ;;
    *)
      invalid_option
      ;;
  esac
}
#}}}
#INSTALL APPLICATION {{{
install_anydesk(){
  package_bulkinstall "gtkglext libglvnd"
  rm -rf anydesk-2.9.5*
  case $ARCHI in
    x86_64)
      wget "${ARCHNIX_DOWNLOADS}/anydesk-2.9.5-amd64.tar.gz" -O anydesk-2.9.5.tar.gz
      tar xvf anydesk-2.9.5.tar.gz
      cp anydesk-2.9.5/anydesk /usr/bin
      ;;
    i*86)
      wget "${ARCHNIX_DOWNLOADS}/anydesk-2.9.5-i686.tar.gz" -O anydesk-2.9.5.tar.gz
      tar xvf anydesk-2.9.5.tar.gz
      cp anydesk-2.9.5/anydesk /usr/bin
      ;;
    *)
      # leave ARCH as-is
      ;;
  esac  
}
#}}}
#INSTALL APPLICATION {{{
install_application(){
  install_qt5
  mkdir -p "/$1"
  chown -R "$username":users /$1
  cd /$1
  case $ARCHI in
    x86_64)
        OSARCH=x64  # or AMD64 or Intel64 or whatever
        ;;
    i*86)
        OSARCH=x86  # or IA32 or Intel32 or whatever
        ;;
    *)
        # leave ARCH as-is
        ;;
  esac
  case $1 in
    posnix)
      chmod 4755 /usr/bin/beep
      su - "${username}" -c "
        rm -rf /$1/install.sh
        rm -rf /$1/PosNix-*.tar.gz
        wget http://archrepo.12yazilim.com:16530/posnix/install.sh -O /$1/install.sh
        chmod u+x /$1/install.sh
        /$1/install.sh $OSARCH
      "
      aur_package_install "teamviewer-quicksupport"
      install_anydesk
      ;;
    posguru)
      if ! is_package_installed "postgresql"; then
        read -p "PostgreSql kurulsun mu (Ana makine ise onerilir) [Y/n]: " OPTION
        case "$OPTION" in
          "n")
            ;;
          "h")
            ;;
          *)
            install_postgresql
            ;;
        esac
      fi
      su - "${username}" -c "
        rm -rf /$1/install.sh
        rm -rf /$1/PosGuru-*.tar.gz
        wget http://archrepo.12yazilim.com:16530/posguru/install.sh -O /$1/install.sh
        chmod u+x /$1/install.sh
        /$1/install.sh $OSARCH
      "
      aur_package_install "teamviewer-quicksupport"
      install_anydesk
      if is_package_installed "postgresql"; then
        checklist[6]=1;
        su - postgres bash -c "psql -c \"CREATE USER posguru WITH PASSWORD '!posguru!123!';\""
        su - postgres bash -c "psql -c \"CREATE DATABASE posgurudb OWNER posguru;\""
        add_line "host    posgurudb    posguru    0.0.0.0/0    md5" "/var/lib/postgres/data/pg_hba.conf"
        chown postgres:postgres /var/lib/postgres/data/pg_hba.conf
        systemctl restart postgresql
      fi
      ;;
    *)
      invalid_option
      ;;
  esac
  install_unclutter
}
#}}}
#INSTALL MARIADB {{{
install_mariadb(){
    package_install "mariadb"
    /usr/bin/mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
    system_ctl enable mysqld.service
    systemctl start mysqld.service
    /usr/bin/mysql_secure_installation
}
#}}}
#INSTALL POSTGRESQL {{{
install_postgresql(){
    package_install "postgresql"
    mkdir -p /var/lib/postgres
    chown -R postgres:postgres /var/lib/postgres
    systemd-tmpfiles --create postgresql.conf
    echo "Enter your new postgres account password:"
    passwd postgres
    while [[ $? -ne 0 ]]; do
      passwd postgres
    done
    su - postgres -c "initdb --locale ${LOCALE}.UTF-8 -D /var/lib/postgres/data"

    replace_line "#listen_addresses" "listen_addresses" "/var/lib/postgres/data/postgresql.conf"
    chown postgres:postgres /var/lib/postgres/data/postgresql.conf

    system_ctl enable postgresql.service
    system_ctl start postgresql.service
    package_install "pgadmin3"
}
#}}}
#FINISH {{{
finish(){
  if [[ ! -f "/root/install_done" ]]; then
    systemctl disable dhcpcd.service
    systemctl disable dhcpcd@.service
    WIRED_DEVS=`ip link | grep "state UP" | grep "ens\|eno\|enp" | awk '{print $2}'| sed 's/://'`
    echo "Found UP wired devices : ${WIRED_DEVS}"
    if [[ -n $WIRED_DEVS ]]; then
      for WIRED_DEV in ${WIRED_DEVS[@]}; do
        echo "Disable DHCPCD Service for ${WIRED_DEV}..."
        systemctl disable dhcpcd@${WIRED_DEV}.service
      done
    fi
    rm -rf /etc/systemd/system/getty@tty1.service.d/override.conf
    echo "[Service]" >> /etc/systemd/system/getty@tty1.service.d/override.conf
    echo "ExecStart=" >> /etc/systemd/system/getty@tty1.service.d/override.conf
    echo "ExecStart=-/usr/bin/agetty -a ${username} --noclear %I $TERM" >> /etc/systemd/system/getty@tty1.service.d/override.conf
    print_title "INSTALL COMPLETED"
    echo -e "Thanks for using the ARCHNIX Install script\n"
    touch /root/install_done
    reboot
  fi
  exit 0
}
#}}}
LOG="/root/`basename ${0}`.log" # LOG FILE
welcome
if [[ ! -f "/root/install_done" ]]; then
  check_root
  check_archlinux
  check_hostname
  check_connection
  timedatectl set-ntp true
  check_pacman_blocked
  check_multilib
  pacman_key
  system_update
  configure_sudo
  init_locale

  package_bulkinstall "base-devel parted btrfs-progs f2fs-tools ntp net-tools openssh yajl namcap wget beep"
  install_nm_wicd

  if is_package_installed "kdebase-workspace"; then KDE=1; fi
  install_basic_setup
  install_video_cards
  install_gui

  package_bulkinstall "$profile_packages"

  rm -rf /root/.bash_profile
  clean_orphan_packages
else
  init_locale
fi

AUR_PKG_MANAGER="yaourt --tmp /var/tmp/"

if is_package_installed "anydesk"; then
  checklist[3]=1;
fi
if is_package_installed "teamviewer"; then
  checklist[4]=1;
fi
if is_package_installed "chromium"; then
  checklist[5]=1;
fi
if is_package_installed "postgresql"; then
  checklist[6]=1;
fi

while true
do
  print_title "ARCHNIX INSTALL / ONIKI YAZILIM"
  print_warning "USERNAME: ${username} , HOSTNAME: ${HOSTNAME} , PROFILE: ${PROFILE}"
  echo " a) $(mainmenu_item "${checklist[11]}" "PosNix Kur")"
  echo " b) $(mainmenu_item "${checklist[12]}" "PosGuru Kur")"
  echo ""
  echo " 1) $(mainmenu_item "${checklist[1]}" "Sistem Dili" "${LOCALE}")"
  echo " 2) $(mainmenu_item "${checklist[2]}" "Tema Paper Kur (! Buyuk !)")"
  echo " 3) $(mainmenu_item "${checklist[3]}" "Anydesk Kur")"
  echo " 4) $(mainmenu_item "${checklist[4]}" "Teamviewer Kur")"
  echo " 5) $(mainmenu_item "${checklist[5]}" "Chromium Kur")"
  echo " 6) $(mainmenu_item "${checklist[6]}" "PostgreSQL Kur")"
  echo ""
  echo "10) $(mainmenu_item "${checklist[10]}" "Gereksiz Paketleri Temizle")"
  if [[ -n ${PROFILE} ]]; then
    profile_print_menuitems 20
  fi
  echo ""
  echo " q) Quit"
  echo ""
  MAINMENU+=" q"
  read_input_options "$MAINMENU"
  for OPT in ${OPTIONS[@]}; do
    case "$OPT" in
      1)
          language_selector
          checklist[1]=1
          ;;
      2)
        install_theme_paper
        checklist[2]=1
	      ;;
      3)
	      install_anydesk
        checklist[3]=1
	      ;;
      4)
	aur_package_install "teamviewer"
        checklist[4]=1
	;;
      5)
	package_install "chromium"
        checklist[5]=1
	;;
      6)
	install_postgresql
        checklist[6]=1
	;;
      "a")
	install_application "posnix"
  checklist[11]=1
	;;
      "b")
	install_application "posguru"
  checklist[12]=1
  ;;
      10)
	clean_orphan_packages
  checklist[10]=1
	;;
      "q")
        finish
        ;;
      *)
        profile_handle_menuitem 20 $OPT
        ;;
    esac
  done
done
#}}}
#SAMBA {{{
install_samba(){
  print_title "SAMBA - https://wiki.archlinux.org/index.php/Samba"
  print_info "Samba is a re-implementation of the SMB/CIFS networking protocol, it facilitates file and printer sharing among Linux and Windows systems as an alternative to NFS."
  read_input_text "Install Samba" $SAMBA
  if [[ $OPTION == y ]]; then
    package_install "samba smbnetfs"
    [[ ! -f /etc/samba/smb.conf ]] && cp /etc/samba/smb.conf.default /etc/samba/smb.conf
    local CONFIG_SAMBA=`cat /etc/samba/smb.conf | grep usershare`
    if [[ -z $CONFIG_SAMBA ]]; then
      # configure usershare
      export USERSHARES_DIR="/var/lib/samba/usershare"
      export USERSHARES_GROUP="sambashare"
      mkdir -p ${USERSHARES_DIR}
      groupadd ${USERSHARES_GROUP}
      chown root:${USERSHARES_GROUP} ${USERSHARES_DIR}
      chmod 1770 ${USERSHARES_DIR}
      sed -i -e '/\[global\]/a\\n   usershare path = /var/lib/samba/usershare\n   usershare max shares = 100\n   usershare allow guests = yes\n   usershare owner only = False' /etc/samba/smb.conf
      sed -i -e '/\[global\]/a\\n   socket options = IPTOS_LOWDELAY TCP_NODELAY SO_KEEPALIVE\n   write cache size = 2097152\n   use sendfile = yes\n' /etc/samba/smb.conf
      usermod -a -G ${USERSHARES_GROUP} ${username}
      sed -i '/user_allow_other/s/^#//' /etc/fuse.conf
      modprobe fuse
    fi
    echo "Enter your new samba account password:"
    pdbedit -a -u ${username}
    while [[ $? -ne 0 ]]; do
      pdbedit -a -u ${username}
    done
    # enable services
    system_ctl enable smbd
    system_ctl enable nmbd
    pause_function
  fi
}
#}}}
#ADDITIONAL FIRMWARE {{{
install_additional_firmwares(){
  print_title "INSTALL ADDITIONAL FIRMWARES"
  read_input_text "Install additional firmwares [Audio,Bluetooth,Scanner,Wireless]" $FIRMWARE
  if [[ $OPTION == y ]]; then
    while true
    do
      print_title "INSTALL ADDITIONAL FIRMWARES"
      echo " 1) $(menu_item "aic94xx-firmware") $AUR"
      echo " 2) $(menu_item "alsa-firmware")"
      echo " 3) $(menu_item "b43-firmware") $AUR"
      echo " 4) $(menu_item "b43-firmware-legacy") $AUR"
      echo " 5) $(menu_item "bfa-firmware") $AUR"
      echo " 6) $(menu_item "bluez-firmware") [Broadcom BCM203x/STLC2300 Bluetooth]"
      echo " 7) $(menu_item "broadcom-wl") $AUR"
      echo " 8) $(menu_item "ipw2100-fw")"
      echo " 9) $(menu_item "ipw2200-fw")"
      echo "10) $(menu_item "libffado") [Fireware Audio Devices]"
      echo "11) $(menu_item "libmtp") [Android Devices]"
      echo "12) $(menu_item "libraw1394") [IEEE1394 Driver]"
      echo ""
      echo " d) DONE"
      echo ""
      FIRMWARE_OPTIONS+=" d"
      read_input_options "$FIRMWARE_OPTIONS"
      for OPT in ${OPTIONS[@]}; do
        case "$OPT" in
          1)
            aur_package_install "aic94xx-firmware"
            ;;
          2)
            package_install "alsa-firmware"
            ;;
          3)
            aur_package_install "b43-firmware"
            ;;
          4)
            aur_package_install "b43-firmware-legacy"
            ;;
          5)
            aur_package_install "bfa-firmware"
            ;;
          6)
            package_install "bluez-firmware"
            ;;
          7)
            aur_package_install "broadcom-wl"
            ;;
          8)
            package_install "ipw2100-fw"
            ;;
          9)
            package_install "ipw2200-fw"
            ;;
          10)
            package_install "libffado"
            ;;
          11)
            package_install "libmtp"
            aur_package_install "android-udev"
            ;;
          12)
            package_install "libraw1394"
            ;;
          "d")
            break
            ;;
          *)
            invalid_option
            ;;
        esac
      done
      source sharedfuncs_elihw
    done
  fi
}
#}}}
