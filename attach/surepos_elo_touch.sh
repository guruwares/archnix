#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

install_elotouch_serial(){
  rm -rf /etc/opt/elo-ser
  echo "Installing EloTouch Serial driver $1"
  tar -xvf `pwd`/attach/surepos_elotouch_serial.tgz
  mkdir -p /etc/opt
  cp -r ./bin-serial/ /etc/opt/elo-ser
  cd /etc/opt/elo-ser
  chmod -R 777 *
  chmod -R 444 *.txt

  rm -rf /etc/sudoers.d/surepos_elotouch_serial
  echo "Cmnd_Alias ELOSER=/etc/opt/elo-ser/setup/loadelo,/etc/opt/elo-ser/eloser,/etc/opt/elo-ser/elovaLite" >> /etc/sudoers.d/surepos_elotouch_serial
  echo "${username} ALL=(ALL) NOPASSWD: ELOSER" >> /etc/sudoers.d/surepos_elotouch_serial

  rm -rf /home/$username/eloser.sh
  echo "sudo /etc/opt/elo-ser/setup/loadelo" >> /home/$username/eloser.sh
  echo "sudo /etc/opt/elo-ser/eloser --handshake $1" >> /home/$username/eloser.sh
  chown "$username":users /home/${username}/eloser.sh
  chmod u+x /home/$username/eloser.sh

  rm -rf /home/$username/.xinitrc.tmp
  mv /home/$username/.xinitrc /home/$username/.xinitrc.tmp
  sed "3i/home/${username}/eloser.sh" /home/$username/.xinitrc.tmp > /home/$username/.xinitrc
  chown "$username":users /home/${username}/.xinitrc

  case $desktop in
    lxde)
      rm -rf /home/${username}/.local/share/applications/elovalite.desktop
      mkdir -p /home/${username}/.local/share/applications
      chown -R "$username":users /home/${username}/.local/share/applications
      echo "[Desktop Entry]" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Type=Application" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Version=1.0" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Name=EloTouch Calibration" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Comment=EloTouch Calibration tool" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Path=/etc/opt/elo-ser" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Exec=sudo /etc/opt/elo-ser/elovaLite" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Terminal=false" >> /home/${username}/.local/share/applications/elovalite.desktop
      echo "Categories=System;" >> /home/${username}/.local/share/applications/elovalite.desktop
      chown "$username":users /home/${username}/.local/share/applications/elovalite.desktop
      ;;
    xfce)
      ;;
    *)
      invalid_option
      ;;
  esac
}
