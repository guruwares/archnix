# PosNix/PosGuru Arch Based Linux Install

Install and configure PosNix/PosGuru has never been easier!

You can try it first with a `virtualbox`

Or download Arch DUAL Iso from https://archive.archlinux.org/iso/2017.01.01/archlinux-2017.01.01-dual.iso

## Prerequisites

- A working internet connection
- Logged in as 'root'

## How to get it
### With git
- Loadkeys for your lang : `loadkeys trq`
- Get the script: `wget http://bit.ly/2K8CCjz -O archnix.tar.gz`
- UnZip it: `tar xvf archnix.tar.gz && cd guruwares*`
- execute PosNix mode installer `./posnix.sh`
- execute PosGuru mode installer `./posguru.sh`
