#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

NOMODESET=(`cat /proc/cmdline | grep "nomodeset" | wc -l`)
IRQPOLL=(`cat /proc/cmdline | grep "irqpoll" | wc -l`)
filesystem="ext4"
swap_type="partition"
partition_layout="Default"
devices_list=(`lsblk -d | awk '{print "/dev/" $1}' | grep 'sd\|hd\|vd\|nvme\|mmcblk' | sort`);
device="${devices_list[0]}";

#ARCHLINUX INSTALL SCRIPTS MODE {{{
#SELECT KEYMAP {{{
select_keymap(){
  op_title="KEYMAP - https://wiki.archlinux.org/index.php/KEYMAP"
	SELECTION=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --menu "$str_keys_msg" 18 60 10 \
	"trq" "Turkish Q" \
  "trf" "Turkish F" \
	"us" "United States" \
	"de" "German" \
	"el" "Greek" \
	"hu" "Hungarian" \
	"es" "Spanish" \
	"fr" "French" \
	"it" "Italian" \
	"pt-latin9" "Portugal" \
	"ro" "Romanian" \
	"ru" "Russian" \
	"sv" "Swedish" \
	"uk" "United Kingdom" 3>&1 1>&2 2>&3)

	if [[ -n $SELECTION ]]; then
    KEYMAP="$SELECTION"
    loadkeys "$KEYMAP"
	fi
}
#}}}
#MIRRORLIST {{{
select_mirrorlist(){
  op_title="MIRRORLIST - https://wiki.archlinux.org/index.php/Mirrors"
  local countries=$(echo -e "AS All-Https\n AT Austria\n AU  Australia\n BE Belgium\n BG Bulgaria\n BR Brazil\n BY Belarus\n CA Canada\n CL Chile \n CN China\n CO Colombia\n CZ Czech-Republic\n DE Germany\n DK Denmark\n EE Estonia\n ES Spain\n FI Finland\n FR France\n GB United-Kingdom\n HU Hungary\n IE Ireland\n IL Isreal\n IN India\n IT Italy\n JP Japan\n KR Korea\n KZ Kazakhstan\n LK Sri-Lanka\n LU Luxembourg\n LV Latvia\n MK Macedonia\n NC New-Caledonia\n NL Netherlands\n NO Norway\n NZ New-Zealand\n PL Poland\n PT Portugal\n RO Romania\n RS Serbia\n RU Russia\n SE Sweden\n SG Singapore\n SK Slovakia\n TR Turkey\n TW Taiwan\n UA Ukraine\n US United-States\n UZ Uzbekistan\n VN Viet-Nam\n ZA South-Africa")
  SELECTION=$(dialog --ok-button "$str_ok" --cancel-button "$str_cancel" --menu "$mirror_msg1" 17 60 10 \
  			"$country_code" "$country_name" \
  			$countries 3>&1 1>&2 2>&3)
  if [[ -n $SELECTION ]]; then
    country_name=${countries[ SELECTION - 1 ]}
    country_code="$SELECTION"
  fi
}
#}}}
#MIRRORLIST {{{
configure_mirrorlist(){
  url="https://www.archlinux.org/mirrorlist/?country=${country_code}&use_mirror_status=on"

  tmpfile=$(mktemp --suffix=-mirrorlist)

  # Get latest mirror list and save to tmpfile
  curl -so ${tmpfile} ${url}
  sed -i 's/^#Server/Server/g' ${tmpfile}

  # Backup and replace current mirrorlist file (if new file is non-zero)
  if [[ -s ${tmpfile} ]]; then
   { echo " Backing up the original mirrorlist..."
     rm -rf /etc/pacman.d/mirrorlist; } &&
   { echo " Rotating the new list ($country_name) into place..."
     mv -i ${tmpfile} /etc/pacman.d/mirrorlist; }
  else
    echo " Unable to update, could not download list."
  fi
  # better repo should go first
  cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.tmp
  rankmirrors /etc/pacman.d/mirrorlist.tmp > /etc/pacman.d/mirrorlist
  rm /etc/pacman.d/mirrorlist.tmp
  #allow global read access (required for non-root yaourt execution)
  chmod +r /etc/pacman.d/mirrorlist
}
#}}}
#SETUP ALTERNATIVE DNS {{{
setup_alt_dns(){
  cat <<- EOF > /etc/resolv.conf.head
# OpenDNS IPv4 nameservers
nameserver 208.67.222.222
nameserver 208.67.220.220
# OpenDNS IPv6 nameservers
nameserver 2620:0:ccc::2
nameserver 2620:0:ccd::2

# Google IPv4 nameservers
nameserver 8.8.8.8
nameserver 8.8.4.4
# Google IPv6 nameservers
nameserver 2001:4860:4860::8888
nameserver 2001:4860:4860::8844

# Comodo nameservers
nameserver 8.26.56.26
nameserver 8.20.247.20

# Basic Yandex.DNS - Quick and reliable DNS
nameserver 77.88.8.8
nameserver 77.88.8.1
# Safe Yandex.DNS - Protection from virus and fraudulent content
nameserver 77.88.8.88
nameserver 77.88.8.2
# Family Yandex.DNS - Without adult content
nameserver 77.88.8.7
nameserver 77.88.8.3

# censurfridns.dk IPv4 nameservers
nameserver 91.239.100.100
nameserver 89.233.43.71
# censurfridns.dk IPv6 nameservers
nameserver 2001:67c:28a4::
nameserver 2002:d596:2a92:1:71:53::
EOF
}
#}}}
#UMOUNT PARTITIONS {{{
umount_partitions(){
  mounted_partitions=(`lsblk | grep ${MOUNTPOINT} | awk '{print $7}' | sort -r`)
  swapoff -a
  for i in ${mounted_partitions[@]}; do
    umount $i
  done
}
#}}}
#SELECT DEVICE {{{
select_device(){
  devices_list=(`lsblk -d | awk '{print "/dev/" $1}' | grep 'sd\|hd\|vd\|nvme\|mmcblk'`);
  PS3="$prompt1"
  echo -e "Attached Devices:\n"
  lsblk -lnp -I 2,3,8,9,22,34,56,57,58,65,66,67,68,69,70,71,72,91,128,129,130,131,132,133,134,135,259 | awk '{print $1,$4,$6,$7}'| column -t
  echo -e "\n"
  echo -e "Select device to partition:\n"
  select device in "${devices_list[@]}"; do
    if contains_element "${device}" "${devices_list[@]}"; then
      break
    else
      invalid_option
    fi
  done
  BOOT_MOUNTPOINT=$device
}
#}}}
#CREATE PARTITION SCHEME {{{
create_partition_scheme(){
  umount_partitions
  echo "Device : ${device}"

  create_partition
}
#}}}
#SETUP PARTITION{{{
create_partition(){
  (
	echo "$PARTITIONTABLETYPE" # Create a new empty DOS partition table
  echo w
  ) | fdisk ${device}

  wipefs -a ${device}

  (
  echo "$PARTITIONTABLETYPE" # Create a new empty DOS partition table
	echo n # Add a new partition
	echo p # Primary partition
	echo 1 # Partition number
	echo   # First sector (Accept default: 1)
  echo +200M
  echo n # Add a new partition
	echo p # Primary partition
	echo 2 # Partition number
	echo   # First sector (Accept default: 1)
	echo +"$SWAPSIZE"  # Last sector (Accept default: varies)
	echo t
  echo 2 # Partition number
	echo 82
	echo n # Add a new partition
	echo p # Primary partition
	echo 3 # Partition number
	echo   # First sector (Accept default: 1)
	echo   # Last sector (Accept default: varies)
	echo a # Add a new partition
	echo 1 # Partition number
	echo w # Write changes
  ) | fdisk ${device}
}
#}}}
#SELECT|FORMAT PARTITIONS {{{
format_partitions(){
  #block_list=(`lsblk | grep 'part' | awk '{print "/dev/" substr($1,3)}'`)

  partition=""${device}1"";
  echo
  echo "Formatting Partition ${partition} as ${filesystem}"
  echo
  mkfs.${filesystem} -F "${partition}" \
        $([[ ${filesystem} == xfs || ${filesystem} == btrfs ]] && echo "-f") \
        $([[ ${filesystem} == vfat ]] && echo "-F32") \
        $([[ $TRIM -eq 1 && ${filesystem} == ext4 ]] && echo "-E discard") \
        $([[ $TRIM -eq 1 && ${filesystem} == btrfs ]] && echo "-O discard")
  echo

  swap_partition="${device}2"
  mkswap ${swap_partition}

  partition=""${device}3"";
  echo "Formatting Partition ${partition} as ${filesystem}"
  echo
  mkfs.${filesystem} -F "${partition}" \
        $([[ ${filesystem} == xfs || ${filesystem} == btrfs ]] && echo "-f") \
        $([[ ${filesystem} == vfat ]] && echo "-F32") \
        $([[ $TRIM -eq 1 && ${filesystem} == ext4 ]] && echo "-E discard") \
        $([[ $TRIM -eq 1 && ${filesystem} == btrfs ]] && echo "-O discard")
  mount_partitions
}
mount_partitions(){
  swap_partition="${device}2"
  swapon ${swap_partition}

  BOOT_MOUNTPOINT="${device}";
  partition=""${device}3"";
  ROOT_PART=`echo ${partition} | sed 's/\/dev\/mapper\///' | sed 's/\/dev\///'`;
  ROOT_MOUNTPOINT=${partition};
  fsck "${partition}"
  mkdir -p "${MOUNTPOINT}"
  mount -t "${filesystem}" "${partition}" "${MOUNTPOINT}"
  mkdir -p ${MOUNTPOINT}/boot
  mount -t "${filesystem}" "${BOOT_MOUNTPOINT}1" "${MOUNTPOINT}/boot"
  mounted=1
}
#}}}
#INSTALL BASE SYSTEM {{{
install_base_system(){
  print_title "INSTALL BASE SYSTEM $1"
  print_info "Using the pacstrap script we install the base system. The base-devel package group will be installed also."
  rm ${MOUNTPOINT}${EFI_MOUNTPOINT}/vmlinuz-linux
  if [[ "$1" == "l" ]]; then
    #https://wiki.archlinux.org/index.php/Archiso#Installation_without_Internet_access
    sed -i 's/en_US\(.\+\)/#en_US\1/g' /etc/locale.gen

    echo "Copying root to ${MOUNTPOINT} ..."
    time cp -ax / ${MOUNTPOINT}
    echo "Copying kernel $(uname -m) to ${MOUNTPOINT}/boot ..."
    cp -aT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz ${MOUNTPOINT}/boot/vmlinuz-linux
    echo "Organizing environment ..."
    sed -i 's/Storage=volatile/#Storage=auto/' ${MOUNTPOINT}/etc/systemd/journald.conf
    rm -rf ${MOUNTPOINT}/etc/udev/rules.d/81-dhcpcd.rules
    arch_chroot "systemctl disable pacman-init.service choose-mirror.service"
    rm -rf ${MOUNTPOINT}/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
    rm -rf ${MOUNTPOINT}/etc/systemd/scripts/choose-mirror
    rm -rf ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/*.conf
    rm -rf ${MOUNTPOINT}/root/{.automated_script.sh,.zlogin}
    rm -rf ${MOUNTPOINT}/etc/mkinitcpio-archiso.conf
    rm -rf ${MOUNTPOINT}/etc/initcpio
    rm -rf ${MOUNTPOINT}/etc/locale.conf
    echo "l" >> ${MOUNTPOINT}/root/install_from
    #a rm -rf ${MOUNTPOINT}/var/cache/pacman
    #a rm -rf ${MOUNTPOINT}/var/lib/pacman
    #a mkdir -p ${MOUNTPOINT}/var/lib/pacman

    #https://wiki.archlinux.org/index.php/Arch_Linux_Archive#How_to_restore_all_packages_to_a_specific_date
    rm -rf ${MOUNTPOINT}/etc/pacman.d/mirrorlist
    #a echo "Server=file:///run/archiso/bootmnt/repos/\$repo/os/\$arch" >> ${MOUNTPOINT}/etc/pacman.d/mirrorlist
    echo "Server=http://archlinux.arkena.net/archive/repos/2017/01/01/\$repo/os/\$arch" >> ${MOUNTPOINT}/etc/pacman.d/mirrorlist
    echo "Server=${ARCHNIX_REPODB}" >> ${MOUNTPOINT}/etc/pacman.d/mirrorlist
    echo "Server=${ARCHNIX_DOWNLOADS}" >> ${MOUNTPOINT}/etc/pacman.d/mirrorlist    
    #echo "Server=${ARCHLINUX_DATEURL}" >> ${MOUNTPOINT}/etc/pacman.d/mirrorlist
    arch_chroot "chmod +r /etc/pacman.d/mirrorlist"

    arch_chroot "chsh -s /bin/bash"
    echo "Updating pacman keys ..."
    arch_chroot "pacman --noconfirm --needed -Sy"
    arch_chroot "pacman-key --init"
    arch_chroot "pacman-key --populate archlinux"
    sed -i "s/SigLevel.*=.*/SigLevel=Never/" ${MOUNTPOINT}/etc/pacman.conf
  else
    pacstrap ${MOUNTPOINT} base base-devel
    #base-devel linux-lts linux-lts-headers kernel="linux-lts"
    [[ $? -ne 0 ]] && error_msg "Installing base system to ${MOUNTPOINT} failed. Check error messages above."
    echo "r" >> ${MOUNTPOINT}/root/install_from
  fi

  local PTABLE=`parted -l | grep "gpt"`
  [[ -n $PTABLE ]] && pacstrap ${MOUNTPOINT} gptfdisk
  WIRELESS_DEV=`ip link | grep wl | awk '{print $2}'| sed 's/://' | sed '1!d'`
  if [[ -n $WIRELESS_DEV ]]; then
    pacstrap ${MOUNTPOINT} iw wireless_tools wpa_actiond wpa_supplicant dialog
  else
    WIRED_DEVS=`ip link | grep "state UP" | grep "ens\|eno\|enp" | awk '{print $2}'| sed 's/://'`
    echo "Found UP wired devices : ${WIRED_DEVS}"
    if [[ -n $WIRED_DEVS ]]; then
      for WIRED_DEV in ${WIRED_DEVS[@]}; do
        echo "Enable DHCPCD Service for ${WIRED_DEV}..."
        arch_chroot "systemctl enable dhcpcd@${WIRED_DEV}.service"
      done
    fi
  fi
  if is_package_installed "espeakup"; then
    pacstrap ${MOUNTPOINT} alsa-utils espeakup brltty
    arch_chroot "systemctl enable espeakup.service"
  fi
}
#}}}
#CONFIGURE KEYMAP {{{
configure_keymap(){
  #ADD KEYMAP TO THE NEW SETUP
  echo "KEYMAP=$KEYMAP" > ${MOUNTPOINT}/etc/vconsole.conf
}
#}}}
#CONFIGURE FSTAB {{{
configure_fstab(){
  genfstab -p ${MOUNTPOINT} >> ${MOUNTPOINT}/etc/fstab
}
#}}}
#CONFIGURE HOSTNAME {{{
configure_hostname(){
  echo "$host_name" > ${MOUNTPOINT}/etc/hostname
  if [[ ! -f ${MOUNTPOINT}/etc/hosts.aui ]]; then
    cp ${MOUNTPOINT}/etc/hosts ${MOUNTPOINT}/etc/hosts.aui
  else
    cp ${MOUNTPOINT}/etc/hosts.aui ${MOUNTPOINT}/etc/hosts
  fi
  arch_chroot "sed -i '/127.0.0.1/s/$/ '${host_name}'/' /etc/hosts"
  arch_chroot "sed -i '/::1/s/$/ '${host_name}'/' /etc/hosts"
}
#}}}
#SELECT TIMEZONE {{{
select_timezone(){
  op_title="TIMEZONE - https://wiki.archlinux.org/index.php/Timezone"
  print_info "In an operating system the time (clock) is determined by four parts: Time value, Time standard, Time Zone, and DST (Daylight Saving Time if applicable)."
  settimezone
}
#}}}
#CONFIGURE TIMEZONE {{{
configure_timezone(){
  arch_chroot "ln -sf /usr/share/zoneinfo/${ZONE}/${SUBZONE} /etc/localtime"
  arch_chroot "sed -i '/#NTP=/d' /etc/systemd/timesyncd.conf"
  arch_chroot "sed -i 's/#Fallback//' /etc/systemd/timesyncd.conf"
  arch_chroot "echo \"FallbackNTP=0.pool.ntp.org 1.pool.ntp.org 0.fr.pool.ntp.org\" >> /etc/systemd/timesyncd.conf"
  arch_chroot "timedatectl set-ntp true "
}
#}}}
#CONFIGURE HARDWARECLOCK {{{
configure_hardwareclock(){
  arch_chroot "hwclock --systohc --${hwclock}"
}
#}}}
#SELECT LOCALE {{{
select_locale(){
  op_title="LOCALE - https://wiki.archlinux.org/index.php/Locale"
  print_info "Locales are used in Linux to define which language the user uses. As the locales define the character sets being used as well, setting up the correct locale is especially important if the language contains non-ASCII characters."
  setlocale
}
#}}}
#CONFIGURE LOCALE {{{
configure_locale(){
  echo 'LANG="'$LOCALE_UTF8'"' > ${MOUNTPOINT}/etc/locale.conf
  arch_chroot "sed -i 's/#'${LOCALE_UTF8}'/'${LOCALE_UTF8}'/' /etc/locale.gen"
  arch_chroot "locale-gen"
}
#}}}
#CONFIGURE MKINITCPIO {{{
configure_mkinitcpio(){
  print_title "MKINITCPIO - https://wiki.archlinux.org/index.php/Mkinitcpio"
  print_info "mkinitcpio is a Bash script used to create an initial ramdisk environment."
  [[ $LUKS -eq 1 ]] && sed -i '/^HOOK/s/block/block keymap encrypt/' ${MOUNTPOINT}/etc/mkinitcpio.conf
  [[ $LVM -eq 1 ]] && sed -i '/^HOOK/s/filesystems/lvm2 filesystems/' ${MOUNTPOINT}/etc/mkinitcpio.conf
  mkdir -p ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d
  rm -rf ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/override.conf
  echo "[Service]" >> ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/override.conf
  echo "ExecStart=" >> ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/override.conf
  echo "ExecStart=-/usr/bin/agetty -a root --noclear %I \$TERM" >> ${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/override.conf

  rm -rf ${MOUNTPOINT}/root/.bash_profile
  echo "cd ~/guruwares* && ./postInstall.sh" >> ${MOUNTPOINT}/root/.bash_profile
  arch_chroot "mkinitcpio -p linux"
  [[ $? -ne 0 ]] && error_msg "Installing initcpio to ${MOUNTPOINT} failed. Check error messages above."
}
#}}}
#INSTALL BOOTLOADER {{{
install_bootloader(){
  print_title "BOOTLOADER - https://wiki.archlinux.org/index.php/Bootloader"
  print_info "The boot loader is responsible for loading the kernel and initial RAM disk before initiating the boot process."
  print_warning "\tROOT Partition: ${ROOT_MOUNTPOINT}"
  if [[ $UEFI -eq 1 ]]; then
    print_warning "\tUEFI Mode Detected"
    #bootloaders_list=("Grub2" "Syslinux" "Systemd" "Skip")
  else
    print_warning "\tBIOS Mode Detected"
    #bootloaders_list=("Grub2" "Syslinux" "Skip")
  fi
  PS3="$prompt1"
  echo -e "Install bootloader:\n"
  case $bootloader in
    Grub2)
      if [[ "$1" == "l" ]]; then
        arch-chroot "pacman -Sy --needed --noconfirm grub"
      else
        pacstrap ${MOUNTPOINT} grub
      fi
      break
      ;;
    Syslinux)
      if [[ "$1" == "l" ]]; then
        arch-chroot "pacman -Sy --needed --noconfirm syslinux gptfdisk"
      else
        pacstrap ${MOUNTPOINT} syslinux gptfdisk
      fi
      break
      ;;
    Systemd)
      break
      ;;
    Skip)
      [[ $UEFI -eq 1 ]] && break || invalid_option
      ;;
    *)
      invalid_option
      ;;
  esac
  [[ $UEFI -eq 1 ]] && arch-chroot "pacman -Sy --needed --noconfirm efibootmgr dosfstools"
}
#}}}
#CONFIGURE BOOTLOADER {{{
configure_bootloader(){
  case $bootloader in
    Grub2)
  		if [[ $LUKS -eq 1 ]]; then
  		  sed -i -e 's/GRUB_CMDLINE_LINUX="\(.\+\)"/GRUB_CMDLINE_LINUX="\1 cryptdevice=\/dev\/'"${LUKS_DISK}"':crypt"/g' -e 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="cryptdevice=\/dev\/'"${LUKS_DISK}"':crypt"/g' ${MOUNTPOINT}/etc/default/grub
  		fi
      if [[ $NOMODESET != "0" ]]; then
  		  sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="\(.\+\)"/GRUB_CMDLINE_LINUX_DEFAULT="\1 nomodeset"/g' ${MOUNTPOINT}/etc/default/grub
  		fi
      if [[ $IRQPOLL != "0" ]]; then
  		  sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="\(.\+\)"/GRUB_CMDLINE_LINUX_DEFAULT="\1 irqpoll"/g' ${MOUNTPOINT}/etc/default/grub
  		fi

      ##http://www.linuxquestions.org/questions/linux-hardware-18/multiport-serial-card-657198/
      sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="\(.\+\)"/GRUB_CMDLINE_LINUX_DEFAULT="\1 8250.nr_uarts=10"/g' ${MOUNTPOINT}/etc/default/grub
      sed -i 's/#GRUB_TERMINAL/GRUB_TERMINAL/g' ${MOUNTPOINT}/etc/default/grub
      replace_line "GRUB_DISTRIBUTOR=.*" "GRUB_DISTRIBUTOR=\"ArchNix\"" ${MOUNTPOINT}/etc/default/grub
      replace_line "GRUB_TIMEOUT=.*" "GRUB_TIMEOUT=2" ${MOUNTPOINT}/etc/default/grub
      replace_line "GRUB_DISABLE_RECOVERY=.*" "GRUB_DISABLE_RECOVERY=false" ${MOUNTPOINT}/etc/default/grub

      if [[ $UEFI -eq 1 ]]; then
  		  arch_chroot "grub-install --target=x86_64-efi --efi-directory=${EFI_MOUNTPOINT} --bootloader-id=arch_grub --recheck"
  		else
  		  arch_chroot "grub-install --target=i386-pc --recheck --debug ${BOOT_MOUNTPOINT}"
  		fi
      [[ $? -ne 0 ]] && error_msg "Installing Grub2 to ${MOUNTPOINT} failed. Check error messages above."

      arch_chroot "grub-mkconfig -o /boot/grub/grub.cfg"
      [[ $? -ne 0 ]] && error_msg "Applying Grub2 to ${MOUNTPOINT} failed. Check error messages above."
      ;;
    Syslinux)
      print_title "SYSLINUX - https://wiki.archlinux.org/index.php/Syslinux"
      print_info "Syslinux is a collection of boot loaders capable of booting from hard drives, CDs, and over the network via PXE. It supports the fat, ext2, ext3, ext4, and btrfs file systems."
      syslinux_install_mode=("[MBR] Automatic" "[PARTITION] Automatic" "Manual")
      PS3="$prompt1"
      echo -e "Syslinux Install:\n"
      select OPT in "${syslinux_install_mode[@]}"; do
        case "$REPLY" in
          1)
            arch_chroot "syslinux-install_update -iam"
            if [[ $LUKS -eq 1 ]]; then
              sed -i "s/APPEND root=.*/APPEND root=\/dev\/mapper\/${ROOT_PART} cryptdevice=\/dev\/${LUKS_DISK}:crypt ro/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            elif [[ $LVM -eq 1 ]]; then
              sed -i "s/sda[0-9]/\/dev\/mapper\/${ROOT_PART}/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            else
              sed -i "s/sda[0-9]/${ROOT_PART}/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            fi
            print_warning "The partition in question needs to be whatever you have as / (root), not /boot."
            pause_function
            $EDITOR ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            break
            ;;
          2)
            arch_chroot "syslinux-install_update -i"
            if [[ $LUKS -eq 1 ]]; then
              sed -i "s/APPEND root=.*/APPEND root=\/dev\/mapper\/${ROOT_PART} cryptdevice=\/dev\/${LUKS_DISK}:crypt ro/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            elif [[ $LVM -eq 1 ]]; then
              sed -i "s/sda[0-9]/\/dev\/mapper\/${ROOT_PART}/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            else
              sed -i "s/sda[0-9]/${ROOT_PART}/g" ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            fi
            print_warning "The partition in question needs to be whatever you have as / (root), not /boot."
            pause_function
            $EDITOR ${MOUNTPOINT}/boot/syslinux/syslinux.cfg
            break
            ;;
          3)
            print_info "Your boot partition, on which you plan to install Syslinux, must contain a FAT, ext2, ext3, ext4, or Btrfs file system. You should install it on a mounted directory, not a /dev/sdXY partition. You do not have to install it on the root directory of a file system, e.g., with partition /dev/sda1 mounted on /boot you can install Syslinux in the syslinux directory"
            echo -e $prompt3
            print_warning "mkdir /boot/syslinux\nextlinux --install /boot/syslinux "
            arch-chroot ${MOUNTPOINT}
            break
            ;;
          *)
            invalid_option
            ;;
        esac
      done
      ;;
    Systemd)
      print_title "SYSTEMD-BOOT - https://wiki.archlinux.org/index.php/Systemd-boot"
      print_info "systemd-boot (previously called gummiboot), is a simple UEFI boot manager which executes configured EFI images. The default entry is selected by a configured pattern (glob) or an on-screen menu. It is included with systemd since systemd 220-2."
      print_warning "\tSystemd-boot heavily suggests that /boot is mounted to the EFI partition, not /boot/efi, in order to simplify updating and configuration."
      gummiboot_install_mode=("Automatic" "Manual")
      PS3="$prompt1"
      echo -e "Gummiboot install:\n"
      select OPT in "${gummiboot_install_mode[@]}"; do
        case "$REPLY" in
          1)
            arch_chroot "bootctl --path=${EFI_MOUNTPOINT} install"
            print_warning "Please check your .conf file"
            partuuid=`blkid -s PARTUUID ${ROOT_MOUNTPOINT} | awk '{print $2}' | sed 's/"//g' | sed 's/^.*=//'`
            if [[ $LUKS -eq 1 ]]; then
              echo -e "title\tArch Linux\nlinux\t/vmlinuz-linux\ninitrd\t/initramfs-linux.img\noptions\tcryptdevice=\/dev\/${LUKS_DISK}:luks root=\/dev\/mapper\/${ROOT_PART} rw" > ${MOUNTPOINT}/boot/loader/entries/arch.conf
            elif [[ $LVM -eq 1 ]]; then
              echo -e "title\tArch Linux\nlinux\t/vmlinuz-linux\ninitrd\t/initramfs-linux.img\noptions\troot=\/dev\/mapper\/${ROOT_PART} rw" > ${MOUNTPOINT}/boot/loader/entries/arch.conf
            else
              echo -e "title\tArch Linux\nlinux\t/vmlinuz-linux\ninitrd\t/initramfs-linux.img\noptions\troot=PARTUUID=${partuuid} rw" > ${MOUNTPOINT}/boot/loader/entries/arch.conf
            fi
            echo -e "default  arch\ntimeout  5" > ${MOUNTPOINT}/boot/loader/loader.conf
            pause_function
            $EDITOR ${MOUNTPOINT}/boot/loader/entries/arch.conf
            $EDITOR ${MOUNTPOINT}/boot/loader/loader.conf
            break
            ;;
          2)
            arch-chroot ${MOUNTPOINT}
            break
            ;;
          *)
            invalid_option
            ;;
        esac
      done
      ;;
  esac
}
#}}}
#SELECT HOSTNAME {{{
select_hostname(){
  op_title="$str_menu_options_hostname"
	while (true)
	  do		## Prompt user to enter hostname check for starting with numbers or containg special char
		host_name=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --inputbox "\n$str_host_msg" 12 55 "${host_name}" 3>&1 1>&2 2>&3 | sed 's/ //g')

		if (<<<$host_name grep "^[0-9]\|[\[\$\!\'\"\`\\|%&#@()+=<>~;:/?.,^{}]\|]" &> /dev/null); then
			dialog --ok-button "$str_ok" --msgbox "\n$str_host_invalid" 10 60
		else
			break
		fi
	done
}
#}}}
#SELECT USERNAME {{{
select_username(){
  op_title="$str_menu_options_username"
	while (true)
	  do		## Prompt user to enter hostname check for starting with numbers or containg special char
		user_name=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --inputbox "\n$str_user_msg" 12 55 "${user_name}" 3>&1 1>&2 2>&3 | sed 's/ //g')

		if (<<<$user_name grep "^[0-9]\|[\[\$\!\'\"\`\\|%&#@()+=<>~;:/?.,^{}]\|]" &> /dev/null); then
			dialog --ok-button "$str_ok" --msgbox "\n$str_user_invalid" 10 60
		else
			break
		fi
	done
}
#}}}
#ROOT PASSWORD {{{
root_password(){
  print_title "ROOT PASSWORD"
  print_warning "Enter your new root password"
  arch_chroot "passwd"
  while [ $? -ne 0 ]; do
    arch_chroot "passwd"
  done
}
#}}}
#CREATE USER {{{
create_user(){
  arch_chroot "useradd -m -g users -G optical,storage,wheel,video,audio,users,power,network,log,uucp,lp -s /bin/bash ${user_name}"
  user_password
}
#}}}
#ROOT PASSWORD {{{
user_password(){
  print_title "$user_name PASSWORD"
  print_warning "Enter ${user_name} password"
  arch_chroot "passwd ${user_name}"
  while [ $? -ne 0 ]; do
    arch_chroot "passwd ${user_name}"
  done
}
#}}}
#FINISH {{{
finish(){
  cp -R `pwd` ${MOUNTPOINT}/root
  print_title "INSTALL COMPLETED"
  umount /dev/sr0
  eject /dev/sr0
  echo "$PROFILE" >> ${MOUNTPOINT}/root/install_profile
  echo "$user_name" >> ${MOUNTPOINT}/root/install_username
  umount_partitions
  reboot
  exit 0
}
#}}}
#MAıNMENu {{{
mainmenu(){
  op_title="$str_menu_main"
  while true
  do
    if [[ $NOMODESET != "0" ]]; then
      echo " Kernel params : nomodeset"
    fi
    if [[ $IRQPOLL != "0" ]]; then
      echo " Kernel params : irqpoll"
    fi
    hddSummary=""
    if [[ $UEFI -eq 1 ]]; then
      hddSummary="UEFI"
    else
      hddSummary="BIOS"
    fi
    if [[ $PARTITIONTABLETYPE=="o" ]]; then
      hddSummary="${hddSummary}-mbr"
    else
      hddSummary="${hddSummary}-gpt"
    fi
  	menu_item=$(dialog --nocancel --ok-button "$str_ok" --default-item "$def_item" --menu "\n$str_menu_main_description\nUser:${user_name}  Hostname:${host_name}  Log:${LOG}\n" 18 80 15 \
  		"$str_menu_main_profile"          "(${PROFILE} ${desktop})" \
  		"$str_menu_main_harddisk"         "(${device} SSD:${TRIM} ${hddSummary} swap(${SWAPSIZE})"  \
      "$str_menu_main_offline"          "$str_menu_main_offline_description" \
  		"$str_menu_main_online"           "LATEST ${country_name} : ${country_code}" \
      "$str_menu_main_options"          "${KEYMAP} ${LOCALE} ${ZONE} ${SUBZONE}" \
      "$str_menu_main_recovery"         "${bootloader}" \
  		"$str_menu_main_done" "$str_menu_main_done_description" 3>&1 1>&2 2>&3)
    case "$menu_item" in
      "$str_menu_main_profile")
        select_profile
      ;;
      "$str_menu_main_harddisk")
        while true
        do
          op_title="Format"
          options_menu_description="Select device and format partitions"
          menu_item=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --menu "$options_menu_description" 10 60 8 \
            "Device"     "(${device})" \
            "Format"     "(fs:${filesystem} swap:${SWAPSIZE})" 3>&1 1>&2 2>&3)
          case "$menu_item" in
            Device)
              umount_partitions
              select_device
              break
            ;;
            Format)
              create_partition_scheme
              format_partitions
              def_item="$str_menu_main_offline"
              break
            ;;
            *)
              break
            ;;
          esac
        done
      ;;
      "$str_menu_main_online")
        if [[ $mounted -eq 0 ]]; then
          echo "${Red}*> Error: ${Yellow} YOU MUST RUN THE $str_menu_main_harddisk FIRST"
          pause_function
          continue
        fi
        configure_mirrorlist
        install_base_system "r"
        configure_keymap
        root_password
        create_user
        setup_alt_dns
        configure_fstab
        configure_hostname
        configure_locale
        configure_timezone
        configure_hardwareclock
        configure_mkinitcpio

        install_bootloader "r"
        configure_bootloader

        def_item="$str_menu_main_done"
      ;;
      "$str_menu_main_offline")
        install_base_system "l"
        configure_keymap
        root_password
        create_user
        setup_alt_dns
        configure_fstab
        configure_hostname
        configure_locale
        configure_timezone
        configure_hardwareclock
        configure_mkinitcpio

        install_bootloader "l"
        configure_bootloader

        def_item="$str_menu_main_done"
      ;;
      "$str_menu_main_options")
        while true
        do
          op_title="Modify Options"
          options_menu_description="Select item to execute"
          menu_item=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --menu "$options_menu_description" 13 60 8 \
            "$str_menu_options_keymap"    "${KEYMAP}" \
            "$str_menu_options_locale"    "${LOCALE}"  \
            "$str_menu_options_timezone"  "${ZONE}/${SUBZONE}" \
            "$str_menu_options_mirror"    "${country_name} : ${country_code}" \
            "$str_menu_options_hostname"  "${host_name}" \
            "$str_menu_options_username"  "${user_name}" 3>&1 1>&2 2>&3)
          case "$menu_item" in
            "$str_menu_options_keymap")
              select_keymap
            ;;
            "$str_menu_options_locale")
              select_locale
            ;;
            "$str_menu_options_timezone")
              select_timezone
            ;;
            "$str_menu_options_mirror")
              select_mirrorlist
            ;;
            "$str_menu_options_hostname")
              select_hostname
            ;;
            "$str_menu_options_username")
              select_username
            ;;
            *)
              break
            ;;
          esac
        done
      ;;
      "$str_menu_main_recovery")
        while true
        do
          op_title="Recovery"
          recovery_menu_description="Select item to execute"
          menu_item=$(dialog --ok-button "$str_ok" --cancel-button "$str_back" --menu "$recovery_menu_description" 19 60 8 \
            "Bootloader"        "(${bootloader})" \
            "Root Password"     "Change root password" \
            "User Password"     "Change user password" \
            "chroot"            "Chroot to drive" 3>&1 1>&2 2>&3)
          case "$menu_item" in
            Bootloader)
              umount_partitions
              mount_partitions
              install_bootloader
              configure_bootloader "p"
            ;;
            "Root Password")
              umount_partitions
              mount_partitions
              root_password
            ;;
            "User Password")
              umount_partitions
              mount_partitions
              user_password
            ;;
            chroot)
              umount_partitions
              mount_partitions
              arch-chroot ${MOUNTPOINT}
              exit 0
            ;;
            *)
              break
            ;;
          esac
        done
      ;;
      "$str_menu_main_done")
        finish
      ;;
    esac
  done
}
#}}}

check_boot_system
check_connection
timedatectl set-ntp true
check_trim
mounted=0
def_item="$str_menu_main_harddisk"
EDITOR="nano"
source profile/automatic.sh
PROFILE="automatic"
[[ -f $LOG ]] && rm -f $LOG
mainmenu
