#!/bin/bash
#-------------------------------------------------------------------------------

source `pwd`/../const.sh
origin="$(pwd)"

#https://unix.stackexchange.com/questions/117988/wget-with-wildcards-in-http-downloads
download_repository(){
  mkdir -p $origin/repos/${ARCH_REPO_DATE}
  cd $origin/repos/${ARCH_REPO_DATE}
  wget -Ne robots=off --cut-dirs=4 --user-agent=Mozilla/5.0 --reject="index.html*" --no-parent --recursive --relative https://archive.archlinux.org/repos/${ARCH_REPO_DATE}/$1/
}
download_repository "core"
download_repository "extra"
download_repository "community"
download_repository "multilib"

#http://linuxpitstop.com/edit-iso-files-using-mkisofs-in-linux/
if [[ ! -f `pwd`/archlinux-2017.01.01-dual.iso ]]; then
  wget https://archive.archlinux.org/iso/2017.01.01/archlinux-2017.01.01-dual.iso
fi
#sudo apt-get install mkisofs
rm -rf tmp
mkdir $origin/tmp
sudo mount -t iso9660 -o loop archlinux-2017.01.01-dual.iso $origin/tmp
cd $origin/tmp
rm -rf build
mkdir $origin/build
tar cf - . | (cd $origin/build; tar xfp -)
sudo umount $origin/tmp
rm -rf $origin/tmp
cd $origin/build
cp -r $origin/repos/${ARCH_REPO_DATE}/archive.archlinux.org $origin/build/repos
mkisofs -o ArchNix-0.1-dual.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -J -R -V "ArchNix 0.1" .

cd $origin
