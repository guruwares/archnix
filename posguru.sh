#!/bin/bash
#-------------------------------------------------------------------------------
# Run this script after your first boot with archlinux (as root)

if [[ -f `pwd`/sharedfuncs.sh ]]; then
  source sharedfuncs.sh
else
  echo "missing file: sharedfuncs"
  exit 1
fi

#ARCHLINUX INSTALL SCRIPTS MODE {{{
#SELECT PROFILE {{{
select_profile(){
  PROFILE=$(dialog --nocancel --menu "\n$title\n\n \Z2*\Zn Select your install hardware profile:" 20 60 10 \
		"automatic" "Automatic" \
		"surepos4840" "IBM SurePos 4840" \
		"surepos4850" "IBM SurePos 4850" 3>&1 1>&2 2>&3)
  if [[ -n $PROFILE ]]; then
    source profile/"$PROFILE".sh;
  fi
}
#}}}

LUKS=0
LVM=0
KEYMAP="trq";
echo Loading keys $KEYMAP
loadkeys "$KEYMAP";
host_name="posguru"
user_name="posguru"
hwclock="localtime";#or utc
LOCALE="tr_TR"
LOCALE_UTF8="${LOCALE}.UTF-8"
ZONE="Europe"
SUBZONE="Istanbul"
country_code="TR"
country_name="Turkey"
bootloader="Grub2"

source _install.sh
