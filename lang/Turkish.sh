#setfont /usr/share/kbd/consolefonts/cp857.16.gz
setfont /usr/share/kbd/consolefonts/lat5-16.psfu.gz

str_ok="Tamam"
str_back="Vazgeç"
str_keys_msg="Klavye Dizilimi Seçimi"
str_locale_msg="Dil Kodlaması Seçimi"
str_backtitle="ArchNix Kurucusu"
str_title="ArchNix Kurucusu"
str_menu_main="Menu"
str_menu_main_description="Buradan ana kurulum / parametre yönetimini yapabilirsiniz."

str_menu_main_profile="Profil"
str_menu_main_harddisk="Format HDD"
str_menu_main_online="Başlat (Online)"
str_menu_main_offline="Başlat (CD/DVD/Flash)"
str_menu_main_offline_description="Hızlı kurulum (Önerilir)"
str_menu_main_options="Opsiyonlar..."
str_menu_main_recovery="Kurtarma..."
str_menu_main_done="İleri"
str_menu_main_done_description="Yeniden başlatıp kuruluma devam et..."

str_menu_options_keymap="Klavye Dili"
str_menu_options_locale="Sistem Dili"
str_menu_options_timezone="Zaman Dilimi"
str_menu_options_mirror="Merkezi Dosya Sunucu Yansısı"
str_menu_options_hostname="Bilgisayar Adı"
str_menu_options_username="Kullanıcı Adı"

str_host_msg="Yeni isimi girin"
str_host_invalid="Geçersiz bilgisayar adı girdiniz"

str_user_msg="Yeni isimi girin"
str_user_invalid="Geçersiz kullanıcı adı girdiniz"
